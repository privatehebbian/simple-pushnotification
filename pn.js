let express = require('express')
let http = require('http')
let path = require('path')
let yaml = require('yamljs')
var socketio = require('socket.io')
let config = yaml.load('./config.yaml')
let Sequelize = require("sequelize")
let app = express()
var io = socketio()
app.io = io
app.disable('x-powered-by')

// ORM SETUP ========================================================
let sequelize = new Sequelize(config.dbconfig.database, config.dbconfig.username, config.dbconfig.password, config.dbconfig)
sequelize.authenticate().then((err) => { }).catch((err) => {
    console.error('Unable to connect to database: ', err.message)
    process.exit(1)
})
let db = {}
let models = ['./peoplemodel.js']
for (idx in models) {
    let model = sequelize.import(models[idx])
    db[model.name] = model
}
Object.keys(db).forEach((mn) => {
    if ('associate' in db[mn]) {
        db[mn].associate(db)
    }
})
db.sequelize = sequelize
db.Sequelize = Sequelize
// ===================================================================

// SERVER INITIALIZATION =============================================
io.on('connection', function(socket){
    console.log('[SOCKET] a user connected: ', socket.id)
    io.to(socket.id).emit('connectioncreated',socket.id)
    let handler = require('./sockethandler')(socket, app)
})

let server = http.createServer(app)
app.io.attach(server)
app.db = db
db.sequelize.sync().then(function () {
    server.listen(config.port, () => {
        console.log('>> DB connected! and server listening on port ' + config.port)
    })
})
// ===================================================================

// ROUTER ============================================================
let sendMsg = (uid, msg, cb) => {
    db.people.findOne({
        where: { uid: uid }
    }).then((result) => {
        if (result) {
            io.to(result.socketid).emit('newmessage', 'This is new message')
            cb(true)
        } else {
            cb(false)
        }
    })
}
app.get('/', (req, res, next) => {
    res.sendStatus(200)
})
app.get('/client', (req, res, next) => {
    res.sendFile(path.join(__dirname+'/client/index.html'))
})
app.get('/send/:uid', (req, res, next) => {
    sendMsg(req.params.uid, 'Lorem ipsum dolor sit amet', (status) => {
        res.sendStatus( (status) ? 200 : 404 )
    })
})
// ===================================================================

module.exports = app
