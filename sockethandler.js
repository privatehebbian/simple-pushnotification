let app = require('./pn')
module.exports = function (socket, app) {
    socket.on('register', function(data){
        console.log('[SOCKET] Received register', data)
        let uid = JSON.parse(data).uid
        let def = {
            uid: uid,
            socketid: socket.id
        }
        app.db.people.findOrCreate({
            where: { uid: uid },
            defaults: def
        }).spread((result, created) => {
            result.update({socketid: socket.id})
            app.io.to(socket.id).emit('register', 'Success registered!')
        })

    })
}
