"use strict";

module.exports = function(sequelize, DataTypes) {
  var people = sequelize.define("people", {
      uid: DataTypes.INTEGER,
      socketid: DataTypes.STRING,
  })
  return people
}
